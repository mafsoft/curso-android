package net.mafsoft.asteroides;

/**
 * @author MAF
 */

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

public class Asteroides extends Activity {

	  public static AlmacenPuntuaciones almacen=new AlmacenPuntuacionesArray();
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        Toast.makeText(this, "onCreate", Toast.LENGTH_SHORT).show();
    }

//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.main, menu);
//        return true;
//    }
    
    public void lanzarAcercaDe(View view){
        Intent i = new Intent(this, AcercaDe.class);
        startActivity(i);
    }
    
    public void lanzarPreferencias(View view){
        Intent i = new Intent(this, Preferencias.class);
        startActivity(i);
    }

    public void lanzarPuntuaciones(View view){
        Intent i = new Intent(this, Puntuaciones.class);
        startActivity(i);
    }
    
    public void lanzarJuego(View view){
        Intent i = new Intent(this, VistaJuego.class);
        startActivity(i);
    }
    
    public void lanzarSalir(View view){
    	finish();
    }
    
    
    @Override public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true; /** true -> el men� ya est� visible */
     }
    
     @Override public boolean onOptionsItemSelected(MenuItem item) {
              switch (item.getItemId()) {
              case R.id.acercaDe:
                     lanzarAcercaDe(null);
                     break;
              case R.id.config:
                    lanzarPreferencias(null);	
                    	 break;
              }
              return true; /** true -> consumimos el item, no se propaga*/
     }
     
     
     
     
     @Override protected void onStart() {
    	   super.onStart();
    	   Toast.makeText(this, "onStart", Toast.LENGTH_SHORT).show();
    	}
    	 
    	@Override protected void onResume() {
    	   super.onResume();
    	   Toast.makeText(this, "onResume", Toast.LENGTH_SHORT).show();
    	}
    	 
    	@Override protected void onPause() {
    	   Toast.makeText(this, "onPause", Toast.LENGTH_SHORT).show();
    	   super.onPause();
    	}
    	 
    	@Override protected void onStop() {
    	   Toast.makeText(this, "onStop", Toast.LENGTH_SHORT).show();
    	   super.onStop();
    	}
    	 
    	@Override protected void onRestart() {
    	   super.onRestart();
    	   Toast.makeText(this, "onRestart", Toast.LENGTH_SHORT).show();
    	}
    	 
    	@Override protected void onDestroy() {
    	   Toast.makeText(this, "onDestroy", Toast.LENGTH_SHORT).show();
    	   super.onDestroy();
    	}
    
}
